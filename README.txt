create dump db with drush
========================
Go to drupal directory
$ cd /home/dev01/pry/kartodromo/docroot
Create and compress dump db
$ drush sql-dump | gzip -c > ../backup/db.sql.gz
To restore db with drush
$ zcat ../backup/db.sql.gz | drush sqlc
