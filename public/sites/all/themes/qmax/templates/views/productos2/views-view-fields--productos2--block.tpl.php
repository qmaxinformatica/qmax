<?php
	$title = $fields['title']->content;
	$imagen = $fields['field_imagen_producto']->content;
	$path = $fields['path']->content;
?>

<li class="span3">
				  <div class="thumbnail">
				  <i class="tag"></i>
					<a href="<?php print $path; ?>"><?php print $imagen; ?></a>
					<div class="caption">
					  <h5><?php print $title; ?></h5>
					  <h4><a class="btn" href="<?php print $path; ?>">VER</a></h4>
					</div>
				  </div>
				</li>