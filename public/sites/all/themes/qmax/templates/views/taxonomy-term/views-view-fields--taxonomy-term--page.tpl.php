<?php
  $imagen = $fields['field_imagen_producto']->content;
  $path = $fields['path']->content;
  $title = $fields['title']->content;
?>
<li class="span3">
			  <div class="thumbnail">
				<a href="<?php print $path; ?>"><?php print $imagen; ?></a>
				<div class="caption">
				  <h5><?php print $title; ?></h5>
				  <p> 
					Mas detalles click 
				  </p>
				   <h4 style="text-align:center"><a class="btn" href="<?php print $path; ?>"><i class="icon-zoom-in"></i></a></h4>
				</div>
			  </div>
			</li>