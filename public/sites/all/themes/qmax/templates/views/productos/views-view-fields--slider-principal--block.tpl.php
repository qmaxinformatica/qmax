<?php
	$title = $fields['title']->content;
	$texto = $fields['field_texto_banner']->content;
	$path = $fields['path']->content;
	$imagen = $fields['field_imagen_banner']->content;
?>
<div class="item">
          <div class="container">
            <a href="<?php print $path; ?>"><?php print $imagen; ?></a>
            <div class="carousel-caption">
                  <h4><?php print $title; ?></h4>
                  <p><?php print $texto; ?></p>
                </div>
          </div>
          </div>