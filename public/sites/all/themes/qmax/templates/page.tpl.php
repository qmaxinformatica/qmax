<div id="header">
<div class="container">
<div id="welcomeLine" class="row">
    <div class="span6">Bienbenido <strong>a Qmax</strong></div>
    <div class="span6">
    <div class="pull-right"> 
    </div>
    </div>
</div>
<!-- Navbar ================================================== -->
<div id="logoArea" class="navbar">
<a id="smallScreen" data-target="#topMenu" data-toggle="collapse" class="btn btn-navbar">
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</a>
  <div class="navbar-inner">
    <a class="brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/></a>
<?php
$node = node_load(152);
$img = $node->field_image['und'][0]['uri'];
$file = $node->field_archivo['und'][0]['uri'];
$img_url = image_style_url('thumbnail', $img);
$file_url = file_create_url($file);
?>
          <a href="<?php print $file_url; ?>" target="_blank">
            <img src="<?php print $img_url; ?>" target="_blank" width="130" height="120" style="margin-top: 30px;"/>
          </a>
    <ul id="topMenu" class="nav pull-right">
            <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
            <?php endif; ?>
            <?php if (!empty($secondary_nav)): ?>
            <?php print render($secondary_nav); ?>
            <?php endif; ?>
            <?php if (!empty($page['navigation'])): ?>
            <?php print render($page['navigation']); ?>
            <?php endif; ?>
    </ul>
  </div>
</div>
</div>
</div>
<!-- Header End====================================================================== -->
<div id="mainBody">
    <div class="container">
    <div class="row">
<!-- Sidebar ================================================== -->
    <div id="sidebar" class="span3">
        <?php print render($page['sidebar_first']); ?>
        <ul id="sideManu" class="nav nav-tabs nav-stacked">
        <?php print render($page['acordion']); ?>
        </ul>
    </div>
<!-- Sidebar end=============================================== -->
    <div class="span9" id="mainCol">
    <div id="white">
    <ul class="breadcrumb">
        <li><a href="http://qmaxinformatica.com/">Inicio</a></li>
        <li class="active"><?php print $title; ?></li>
    </ul>
                    <?php print render($title_suffix); ?>
                    <?php print $messages; ?>
                    <?php if (!empty($tabs)): ?>
                    <?php print render($tabs); ?>
                    <?php endif; ?>
                    <?php if (!empty($page['help'])): ?>
                    <?php print render($page['help']); ?>
                    <?php endif; ?>
                    <?php if (!empty($action_links)): ?>
                    <ul class="action-links"><?php print render($action_links); ?></ul>
                    <?php endif; ?>
                    <?php print render($page['content']); ?>
    </div>
</div>
</div></div>
</div>
<!-- MainBody End ============================= -->
<!-- Footer ================================================================== -->
    <div  id="footerSection">
    <div class="container">
<!--
    <div class="row">
-->
<!--
    aqui estaba botton1  
-->
<!--
      <div class="span3">
        <?php //print render($page['botton2']); ?>
      </div>
      <div class="span3">
        <?php //print render($page['botton3']); ?> 
      </div>
      <div id="socialMedia" class="span3 pull-right">
        <?php //print render($page['botton4']); ?>
      </div> 
-->
<!--
    </div>
-->
    <div class="span333" style="width:100%;">
        <?php print render($page['botton1']); ?>
    </div>
<!--
    <p class="pull-right">&copy; LBS Designs</p>
-->
    </div><!-- Container End -->
    </div>    
    <!-- Themes switcher section ============================================================================================= -->
<span id="themesBtn"></span>
